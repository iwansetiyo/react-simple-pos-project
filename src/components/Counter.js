import React from 'react'
import styled from 'styled-components'

const CounterStyle = styled.div`
	width: 1.2rem;
	background: ${props => props.theme.secondary};
	color: #fff;
	border-radius: 50%;
	justify-content: center;
	align-items: center;
	cursor: pointer;
`

const Counter = ({ inc, dec }) => {
	if(inc) {
		return <CounterStyle onClick={inc}>+</CounterStyle>
	}
	else {
		return <CounterStyle onClick={dec}>-</CounterStyle>
	}
}

export default Counter