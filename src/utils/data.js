export const products = [
	{
		id: 1,
		name: "Noodles with Tomato",
		image: require("../assets/foods/makanan1.png"),
		category: "Makanan",
		price: 13000

	},
	{
		id: 2,
		name: "Noodles with Tomato",
		image: require("../assets/foods/makanan2.png"),
		category: "Makanan",
		price: 23000

	},
	{
		id: 3,
		name: "Noodles with Tomato",
		image: require("../assets/foods/makanan3.png"),
		category: "Makanan",
		price: 33000

	},
	{
		id: 4,
		name: "Noodles with Tomato",
		image: require("../assets/foods/makanan4.png"),
		category: "Makanan",
		price: 43000

	},
	{
		id: 5,
		name: "Noodles with Tomato",
		image: require("../assets/foods/makanan5.png"),
		category: "Makanan",
		price: 53000

	},
	{
		id: 6,
		name: "Noodles with Tomato",
		image: require("../assets/foods/makanan6.png"),
		category: "Makanan",
		price: 63000

	},
	{
		id: 7,
		name: "Noodles with Tomato",
		image: require("../assets/foods/makanan7.png"),
		category: "Makanan",
		price: 73000

	},
	{
		id: 8,
		name: "Noodles with Tomato",
		image: require("../assets/foods/makanan8.jpg"),
		category: "Makanan",
		price: 83000

	},
	{
		id: 9,
		name: "Noodles with Tomato",
		image: require("../assets/foods/makanan9.png"),
		category: "Makanan",
		price: 93000

	},
	{
		id: 10,
		name: "Noodles with Tomato",
		image: require("../assets/foods/makanan10.png"),
		category: "Makanan",
		price: 103000

	},
]